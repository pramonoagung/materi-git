# PERINTAH DASAR GIT

1. Membuat repository git
	```
	git init
	```
2. Memilih perubahan yang akan disimpan
	```
	git add....
	git add -p (menyimpan perubahan ke stage secara parsial/sortir/part)
	akan muncul prompt dengan opsi:
	y:simpan semua perubahan
	n:tidak simpan semua perubahan
	q:keluar
	s:split
	```
3. Menyimpan Perubahan
   ```
	git commit
	git commit -m "pesan/note/keterangan perubahan"
   ```	
4. Melihat history perubahan
	```
	git log (versi log lengkap)
	git log --oneline (versi log singkat)
	```   
4. Melihat kondisi perubahan
	```
	git status
	```
5. Membandingkan detail perubahan
	```
	git diff (melihat perubahan yg belum di stage)
	git diff --staged (melihat perubahan yg sudah ada di stage dan belum di commit)
	git diff idcommit1.....idCommit2 (melihat perbedaan antar versi commit)
	```	
5. Undo perubahan ke staging
	```
	git reset HEAD namaFile
	```	
5. Undo
	```
	git reset --hard
	git checkout -- <nama file> (mengembalikan kondisi file yang sudah dicommit menjadi seperti kondisi working area)
	git checkout master (kembalikan ke kondisi paling depan)
	git checkout HEAD~1 (mundur satu langkah)

	Tidak jadi stage : git reset HEAD
	Tidak jadi commit : git reset --mixed HEAD~1
	Tidak jadi commit dan add : git reset --soft HEAD~1
	Tidak jadi commit,add,dan edit : git reset --hard HEAD~1
	```	
6. Melakukan Reset perubahan
	
	* git reset soft
		```
		git reset --soft idCommit/nama branch /HEAD~1 
		(biasanya digunakan untuk edit commit message karena perintah hanya memindahkan head commit)
		```
	* git reset mixed (default mode reset : pindah HEAD dan staging)
		```
		git reset HEAD~1
		digunakan untuk memperbaiki susunan/apa saja yg akan dicommit 
		```

	* git reset hard (hapus semua perubahan dan commit ke kondisi terkahir commit)
		```
		git reset --hard HEAD~1
		git reset --hard idCommit
		```

6. Mengambil perubahan tertentu/Cherry Pick
	```
	git cherry-pick idCommit
	``` 
7. Membuat branch baru
	```
	git branch -b namabranch
	```
8. Pindah ke branch tertentu
   ```
   git checkout namaBranch
   git checkout -b namaBranch (perintah buat branch baru dan pindah ke branch tsb)
   ```
9. Merge dari branch lain
   ```
   git merge namaBranch/idCommit
   git merge --squash namaBranch (gabungkan topic branch jadi satu commit/satu titik)
   ```
10. Menghapus remote branch
	```
	git push origin -d nama-remote-branch
	```